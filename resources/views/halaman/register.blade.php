<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="=X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign Up</title>
</head>

<body>
    <h1>Buat Account Baru!</h1>

    <h3>Sign Up Form</h3>
    <form action="/welcome" method="get">
        @csrf
        <label>First Name:</label><br>
        <input type="text" name="nama"><br><br>
        <label>Last Name:</label><br>
        <input type="text" name="lastname"><br><br>

        <label>Gener:</label><br>
        <input type="radio" name="gender" value="1">Male<br>
        <input type="radio" name="gender" value="2">Female<br>
        <input type="radio" name="gender" value="3">Other<br><br>

        <label>Nationality:</label><br>
        <select name="Nationality">
            <option value="1">Indonesian</option>
            <option value="2">Singapure</option>
            <option value="3">Japan</option>
        </select><br><br>
        
        <label>Languege Spoken</label><br>
        <input type="checkbox" name="bahasa" value="1">Bahasa Indonesia<br>
        <input type="checkbox" name="bahasa" value="2">English<br>
        <input type="checkbox" name="bahasa" value="3">Other<br><br>

        <label>Bio</label><br>
        <textarea name="bio" col="30" rows="10"></textarea><br>
        <input type="submit" value="sign up">
    </form>
</body>
</html>